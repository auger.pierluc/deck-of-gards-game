# Deck of cards game

How to start the game application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/deck-of-cards-game-1.0-SNAPSHOT.jar server game.yml`
1. Api binds on port 8080 `http://localhost:8080`
1. Helper and stats binds on port 8081 `http://localhost:8081`

Simulator
---
To setup a fake game, POST `http://localhost:8080/simulator/setupFakeGame`


Api documentation
---
- Swagger spec is exposed at `http://localhost:8080/swagger.json`
- SwaggerUI is also available at `http://localhost:8080/swagger`