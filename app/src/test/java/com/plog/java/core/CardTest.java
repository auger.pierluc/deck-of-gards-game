package com.plog.java.core;

import org.junit.Test;

import java.security.InvalidParameterException;

import static com.google.common.truth.Truth.assertThat;

public class CardTest {


    Card JACK_SPADE = new Card(11, Suit.spade);
    Card TEN_SPADE = new Card(10, Suit.spade);
    Card JACK_DIAMOND = new Card(11, Suit.diamond);

    @Test
    public void givenCardsFrom1To13_whenNew_thenCardHasValueMatchingNumber() {
        for (int i = 1; i <= 13; i++) {
            final Card card = new Card(i, Suit.club);

            assertThat(i).isEqualTo(card.getValue());
            assertThat(Suit.club).isEqualTo(card.getSuit());
        }
    }

    @Test(expected = InvalidParameterException.class)
    public void givenCard14_whenNew_thenExceptionIsThrown() {
        final Card card = new Card(14, Suit.club);
    }

    @Test
    public void givenTwoCardsOfSameValueAndSuite_equals_isTrue() {
        assertThat(new Card(11, Suit.diamond)).isEqualTo(JACK_DIAMOND);
    }

    @Test
    public void givenTwoCardsDifferentValueAndSameSuite_equals_isFalse() {
        assertThat(TEN_SPADE).isNotEqualTo(JACK_SPADE);
    }

    @Test
    public void givenTwoCardsOfSameValueButDifferentSuite_equals_isFalse() {
        assertThat(JACK_DIAMOND).isNotEqualTo(JACK_SPADE);
    }
}