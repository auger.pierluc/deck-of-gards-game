package com.plog.java.core.game;

import com.plog.java.core.deck.CardCount;
import com.plog.java.core.deck.Deck;
import com.plog.java.core.game.exceptions.NotEnoughCardsLeftToDealException;
import com.plog.java.core.game.exceptions.PlayerAlreadyInGameException;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;

public class GameTest {
    public static final String NAME = "name";

    private static final Player PLAYER_ONE = new Player(NAME);
    private static final Player PLAYER_TWO = new Player(NAME);
    private static final Player PLAYER_THREE = new Player(NAME);
    private final Player player = new Player(NAME);

    @Test
    public void givenNewGame_whenGetPlayers_thenEmptyListIsReturned() {
        final Collection<Player> players = new Game().getPlayers();

        assertThat(players.size()).isEqualTo(0);
    }

    @Test
    public void givenNewGame_whenAddPlayer_thenPlayerIsAddedToGamePlayers() throws PlayerAlreadyInGameException {
        final Game game = new Game();

        game.addPlayer(new Player(NAME));

        assertThat(game.getPlayers().size()).isEqualTo(1);
    }

    @Test(expected = PlayerAlreadyInGameException.class)
    public void givenGameWithPlayer_whenAddSamePlayer_thenExceptionIsThrown() throws PlayerAlreadyInGameException {
        final Player playerAlreadyInGame = new Player(NAME);
        final HashMap<UUID, Player> players = new HashMap<>();
        players.put(playerAlreadyInGame.getId(), playerAlreadyInGame);
        final Game game = new Game();

        game.addPlayer(playerAlreadyInGame);
        game.addPlayer(playerAlreadyInGame);
    }

    @Test(expected = NotEnoughCardsLeftToDealException.class)
    public void givenNoDeck_whenDeal1_thenExceptionIsThrown() throws PlayerAlreadyInGameException, PlayerNotInGameException, NotEnoughCardsLeftToDealException {
        final Game game = new Game();
        game.addPlayer(player);

        game.dealCardsToPlayer(1, player.getId());
    }

    @Test
    public void givenGameWithDeck_whenDeal52_then52CardsCanBeDealt() throws PlayerNotInGameException, PlayerAlreadyInGameException, NotEnoughCardsLeftToDealException {
        final Game game = new Game();
        game.addPlayer(player);
        game.addDeck(new Deck());

        game.dealCardsToPlayer(52, player.getId());

        //no exception thrown
    }

    @Test
    public void givenGameWith2Decks_whenDeal104_then104CardsCanBeDealt() throws PlayerNotInGameException, PlayerAlreadyInGameException, NotEnoughCardsLeftToDealException {
        final Game game = new Game();
        game.addPlayer(player);
        game.addDeck(new Deck());
        game.addDeck(new Deck());

        game.dealCardsToPlayer(104, player.getId());

        //no exception thrown
    }

    @Test(expected = NoDeckInGameException.class)
    public void givenNoDeck_whenDeal_thenExceptionIsThrown() throws NotEnoughCardsLeftToDealException, PlayerNotInGameException {
        final Game game = new Game();

        game.dealCardsToPlayer(1, UUID.randomUUID());
    }


    @Test
    public void getPlayerScores_returnsEmptyList() {
        final Game game = new Game();

        assertThat(game.getPlayersScore()).isEmpty();
    }

    @Test
    public void givenGameWithMultiplePlayerAndCardsDealtToEachOfThem_getPlayerScores_returnsPlayerScoresSortedDecreasing() throws PlayerAlreadyInGameException, NotEnoughCardsLeftToDealException, PlayerNotInGameException {
        final Game game = new Game();
        game.addDeck(new Deck());
        game.addPlayer(PLAYER_ONE);
        game.addPlayer(PLAYER_TWO);
        game.addPlayer(PLAYER_THREE);
        game.dealCardsToPlayer(3, PLAYER_ONE.getId());
        game.dealCardsToPlayer(3, PLAYER_TWO.getId());
        game.dealCardsToPlayer(3, PLAYER_THREE.getId());

        final List<PlayerScore> playersScore = game.getPlayersScore();

        for (int i = 0; i < playersScore.size() - 1; i++) {
            assertThat(playersScore.get(i).getScore()).isGreaterThan(playersScore.get(i + 1).getScore());
        }
    }

    @Test
    public void givenGameWith2Decks_whenGetCardsLeft_thenAllCardsAreReturnedOrderedWithCount2() throws NoDeckInGameException {
        final Game game = new Game();
        game.addDeck(new Deck());
        game.addDeck(new Deck());

        final List<CardCount> cardsLeft = game.getCardsLeftInGame();

        assertThat(cardsLeft.size()).isEqualTo(52);
        for (final CardCount cardCount : cardsLeft) {
            assertThat(cardCount.getCount()).isEqualTo(2);
        }
    }

    @Test(expected = PlayerNotInGameException.class)
    public void givenNoPlayerInGame_whenRemovePlayer_thenException() throws PlayerNotInGameException {
        final Game game = new Game();

        game.removePlayer(UUID.randomUUID());
    }

    @Test
    public void givenPlayerInGame_whenRemovePlayer_thenPlayerIsRemoved() throws PlayerNotInGameException, PlayerAlreadyInGameException {
        final Game game = new Game();
        final Player player = new Player(NAME);
        game.addPlayer(player);

        game.removePlayer(player.getId());

        assertThat(game.getPlayers()).isEmpty();
    }
}