package com.plog.java.core;

import com.plog.java.core.deck.CardCount;
import com.plog.java.core.deck.Deck;
import com.plog.java.core.deck.SuitCount;
import com.plog.java.core.game.exceptions.NotEnoughCardsLeftToDealException;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;

public class DeckTest {
    @Test
    public void givenNewDeck_dealCards_then52CardsCanBeDealt() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();

        deck.dealCards(52);
    }

    @Test(expected = NotEnoughCardsLeftToDealException.class)
    public void givenNewDeck_whenDeal53_thenExceptionIsThrown() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();

        deck.dealCards(53);
    }

    @Test
    public void whenDeal52Cards_thenReturnAllDifferentCards() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();
        final List<Card> cards = deck.dealCards(52);
        final Set<Card> set = new HashSet<Card>(cards);

        assertThat(52).isEqualTo(set.size());
    }

    @Test
    public void givenDeckWithAnotherDeck_whenDeal104Cards_thenDealtCardYields52UniqueCards() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();
        deck.add(new Deck());

        final List<Card> cards = deck.dealCards(104);

        assertThat(52).isEqualTo(cards.stream().distinct().count());
    }

    @Test(expected = NotEnoughCardsLeftToDealException.class)
    public void givenAddedDeck_whenDeal105_thenExceptionIsThrown() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();
        deck.add(new Deck());

        final List<Card> cards = deck.dealCards(105);
    }

    @Test
    public void givenNewDeck_whenDeal1_then51CardAreLeftInDeck() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();

        final List<Card> cards = deck.dealCards(1);

        assertThat(51).isEqualTo(deck.getCards().size());
    }

    @Test
    public void givenNewDeck_whenDeal1_thenDealtCardIsNotInDeckAnymore() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();

        final List<Card> cards = deck.dealCards(1);

        assertThat(deck.getCards().contains(cards.get(0))).isFalse();
    }


    @Test
    public void givenNewDeck_whenDeal3_then49CardAreLeftInDeck() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();

        final List<Card> cards = deck.dealCards(3);

        assertThat(49).isEqualTo(deck.getCards().size());
    }

    @Test
    public void givenNewDeck_whenDeal3_thenDealtCardsAreNotInDeckAnymore() throws NotEnoughCardsLeftToDealException {
        final Deck deck = new Deck();

        final List<Card> cards = deck.dealCards(3);

        assertThat(deck.getCards().contains(cards.get(0))).isFalse();
        assertThat(deck.getCards().contains(cards.get(1))).isFalse();
        assertThat(deck.getCards().contains(cards.get(2))).isFalse();
    }

    @Test
    public void givenTwoIdenticalDecks_whenDeal_then52AreDifferentsDealtAreSameOrder() throws NotEnoughCardsLeftToDealException {
        final Deck firstDeck = new Deck();
        final Deck secondDeck = new Deck();

        assertThat(firstDeck.dealCards(52)).isEqualTo(secondDeck.dealCards(52));
    }

    @Test
    public void givenTwoIdenticalDecks_whenShuffleAndDeal52_then52DealtCardsAreDifferentsCards() throws NotEnoughCardsLeftToDealException {
        final Deck firstDeck = new Deck();
        final Deck secondDeck = new Deck();

        firstDeck.shuffle();
        secondDeck.shuffle();

        assertThat(firstDeck.dealCards(52)).isNotEqualTo(secondDeck.dealCards(52));
    }

    @Test
    public void givenNewDeck_getUndealtCards_returnsCardsLeftPerSuit() {
        final Deck deck = new Deck();

        final List<SuitCount> nonDealtCardsPerSuit = deck.getNonDealtCardsPerSuit();

        for (final SuitCount suitCount : nonDealtCardsPerSuit) {
            assertThat(suitCount.getCount()).isEqualTo(Deck.NUMBER_OF_CARDS_PER_SUITE);
        }
    }


    @Test
    public void givenNewDeck_getRemainingCards_returnsCardsSortedBySuiteAndThenByValueDescending() {
        final Deck deck = new Deck();

        final List<CardCount> remainingCards = deck.getRemainingCardsCount();
        final CardCount first = remainingCards.iterator().next();

        assertThat(first.getCard()).isEqualTo(new Card(13, Suit.heart));
        assertThat(first.getCount()).isEqualTo(1);
    }

    @Test
    public void givenNewDeckWithSecondDeck_getRemainingCards_returnsCardsSortedBySuiteAndThenByValueDescendingWithCount2() {
        final Deck deck = new Deck();
        deck.add(new Deck());
        final List<CardCount> remainingCards = deck.getRemainingCardsCount();
        final CardCount first = remainingCards.iterator().next();

        assertThat(first.getCard()).isEqualTo(new Card(13, Suit.heart));
        assertThat(first.getCount()).isEqualTo(2);
    }
}