package com.plog.java.core;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class SuitTest {
    @Test
    public void orderIsHeartSpadeClubDiamond() {
        assertThat(Suit.values()).isEqualTo(new Suit[]{
                Suit.heart, Suit.spade, Suit.club, Suit.diamond
        });
    }
}