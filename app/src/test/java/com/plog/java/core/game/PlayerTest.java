package com.plog.java.core.game;

import com.plog.java.core.Card;
import com.plog.java.core.Suit;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class PlayerTest {
    public static final String NAME = "name";

    public static final Card TWO_SPADE = new Card(2, Suit.spade);
    private final Card[] cards = new Card[]{new Card(1, Suit.spade), new Card(2, Suit.spade)};
    public static final Card ONE_SPADE = new Card(1, Suit.spade);

    @Test
    public void getScore_thenScoreIs0() {
        final Player player = new Player(NAME);

        final int actual = player.getScore();

        assertThat(0).isEqualTo(actual);
    }

    @Test
    public void given1and2Received_whenGetScore_thenScoreIs3() {
        final Player player = new Player(NAME);

        player.receiveCards(Arrays.asList(cards));

        assertThat(3).isEqualTo(player.getScore());
    }

    @Test
    public void given1and2and10and12_whenGetScore_thenScoreIs25() {
        final Player player = new Player(NAME);

        player.receiveCards(Arrays.asList(cards));
        player.receiveCards(Arrays.asList(new Card[]{new Card(10, Suit.spade), new Card(12, Suit.spade)}));

        assertThat(25).isEqualTo(player.getScore());
    }

    @Test
    public void givenPlayerWithoutCards_whenGetCards_thenEmptyListIsReturned() {
        final Player player = new Player(NAME);

        final List<Card> actual = player.getCards();

        assertThat(actual).isEmpty();
    }

    @Test
    public void givenPlayerReceivingTwoCards_whenGetCards_thenReceivedCardsAreReturned() {
        final Player player = new Player(NAME);
        player.receiveCards(new ArrayList<Card>() {
            {
                add(ONE_SPADE);
                add(TWO_SPADE);
            }
        });

        final List<Card> actual = player.getCards();

        assertThat(actual).contains(ONE_SPADE);
        assertThat(actual).contains(TWO_SPADE);
        assertThat(actual.size()).isEqualTo(2);
    }
}