package com.plog.java.resources;

import com.plog.java.GameApplication;
import com.plog.java.GameConfiguration;
import com.plog.java.api.AddDeckToGameDto;
import com.plog.java.api.deck.CreateDeckDto;
import com.plog.java.api.game.CreateGameDto;
import com.plog.java.api.game.players.AddPlayerToGameDto;
import com.plog.java.api.game.players.DealCardDto;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;

public class GameAcceptanceTest {
    private static final String GAME_NAME = "gameId";
    @Rule
    public final DropwizardAppRule<GameConfiguration> RULE =
            new DropwizardAppRule<>(GameApplication.class, ResourceHelpers.resourceFilePath("game.yml"));
    private JerseyClient client;

    @Before
    public void setup() {
        client = new JerseyClientBuilder().build();

    }

    @Test
    public void givenNoGame_addGame_willReturn201() {

        final Response response = addGame();

        final int status = response.getStatus();
        assertThat(status).isEqualTo(201);
    }


    @Test
    public void givenNoGame_get_willReturn404() {

        final Response response = client.target(
                String.format("http://localhost:%d/games/1c73982d-d141-47c3-b42d-98f1118ae990", RULE.getLocalPort())).request().get();

        assertThat(response.getStatus()).isEqualTo(404);
    }

    @Test
    public void givenNoGame_delete_willReturn404() {

        final Response response = client.target(
                String.format("http://localhost:%d/games/1c73982d-d141-47c3-b42d-98f1118ae990", RULE.getLocalPort())).request().delete();

        assertThat(response.getStatus()).isEqualTo(404);
    }

    @Test
    public void givenGameAdded_deleteGame_willReturn204() {

        final Response addResponse = addGame();

        final Response response = client.target(
                String.format(addResponse.getLocation().toString(), RULE.getLocalPort())).request().delete();

        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    public void givenGameAdded_addPlayer_willReturn201() {
        final Response addResponse = addGame();

        final Response response = addPlayer(addResponse);

        assertThat(response.getStatus()).isEqualTo(201);
    }


    @Test
    public void givenGameAdded_whenDeletePlayer_willReturn404() {
        final Response addResponse = addGame();

        final Response deleteResponse = client.target(
                String.format(addResponse.getLocation().toString() + "/players/" + UUID.randomUUID().toString(), RULE.getLocalPort()))
                .request().delete();

        assertThat(deleteResponse.getStatus()).isEqualTo(404);
    }

    @Test
    public void givenGameAdded_whenAddDeckToGame_willReturn204() {
        final Response response = givenGameWithDeck();

        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    public void givenGameWithDeck_getStats_willReturn200() {
        final Response addDeck = addDeck();
        final Response addGame = addGame();
        addDeckToGame(addDeck, addGame);

        final Response response = client.target(
                String.format(addGame.getLocation().toString() + "/deck/stats", RULE.getLocalPort())).request().get();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    public void givenGameWithDeck_getCards_willReturn200() {
        final Response addDeck = addDeck();
        final Response addGame = addGame();
        addDeckToGame(addDeck, addGame);

        final Response response = client.target(
                String.format(addGame.getLocation().toString() + "/deck/cards", RULE.getLocalPort())).request().get();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    public void givenGameWithDeck_shuffle_willReturn200() {
        final Response addDeck = addDeck();
        final Response addGame = addGame();
        addDeckToGame(addDeck, addGame);

        final Response response = client.target(
                String.format(addGame.getLocation().toString() + "/deck/shuffle", RULE.getLocalPort())).request().post(Entity.json(""));

        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    public void givenGameWithDeckWithPlayers_shuffleThenDealThenDeal() {
        final Response addDeck = addDeck();
        final Response addGame = addGame();
        addDeckToGame(addDeck, addGame);
        final Response addPlayer = addPlayer(addGame);

        final Response response = client.target(
                String.format(addGame.getLocation().toString() + "/deck/shuffle", RULE.getLocalPort())).request().post(Entity.json(""));
        client.target(
                String.format(addGame.getLocation().toString() + "/deck/" + getPlayerId(addPlayer), RULE.getLocalPort())).request().post(Entity.entity(new DealCardDto(3), MediaType.APPLICATION_JSON_TYPE));

    }

    private Response givenGameWithDeck() {
        final Response addResponse = addGame();
        final Response deckResponse = addDeck();
        return addDeckToGame(deckResponse, addResponse);
    }

    private Response addPlayer(final Response addGameResponse) {
        return client.target(
                String.format(addGameResponse.getLocation().toString() + "/players", RULE.getLocalPort()))
                .request().post(Entity.entity(new AddPlayerToGameDto(), MediaType.APPLICATION_JSON_TYPE));
    }

    private String getPlayerId(final Response addPlayerResponse) {
        final String path = addPlayerResponse.getLocation().getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

    private Response addDeckToGame(final Response addDeck, final Response addGame) {
        final String path = addDeck.getLocation().getPath();
        final String deckId = path.substring(path.lastIndexOf('/') + 1);
        return client.target(
                String.format(addGame.getLocation().toString() + "/deck", RULE.getLocalPort()))
                .request().post(Entity.entity(new AddDeckToGameDto(deckId), MediaType.APPLICATION_JSON_TYPE));
    }

    private Response addDeck() {
        return client.target(
                String.format("http://localhost:%d/decks", RULE.getLocalPort()))
                .request().post(Entity.entity(new CreateDeckDto(), MediaType.APPLICATION_JSON_TYPE));
    }

    private Response addGame() {
        return client.target(
                String.format("http://localhost:%d/games", RULE.getLocalPort()))
                .request().post(Entity.entity(new CreateGameDto(GAME_NAME), MediaType.APPLICATION_JSON_TYPE));
    }
}
