package com.plog.java.db;

import com.plog.java.core.game.Game;
import com.plog.java.core.game.exceptions.EntityAlreadyExistsException;
import com.plog.java.core.game.exceptions.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;

public class GamesInMemoryRepositoryTest {

    private EntityInMemoryRepository repository;

    @Before
    public void setup() {
        repository = new GamesInMemoryRepository();
    }

    @Test
    public void givenNoGame_whenAdd_thenGameIsAdded() throws EntityNotFoundException, EntityAlreadyExistsException {
        final Game game = new Game();

        repository.add(game);

        assertThat(game).isEqualTo(repository.get(game.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void givenNoGame_whenGetGame_thenExceptionIsThrown() throws EntityNotFoundException {
        repository.get(UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void givenNoGame_whenDeleteGame_thenExceptionIsThrown() throws EntityNotFoundException {
        repository.delete(UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void givenGameAdded_whenDeleteGameThenGetGame_thenExceptionIsThrown() throws EntityNotFoundException, EntityAlreadyExistsException {
        final Game game = new Game();
        repository.add(game);

        repository.delete(UUID.randomUUID());
        repository.get(UUID.randomUUID());
    }
}