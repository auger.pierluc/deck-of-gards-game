package com.plog.java.db;

import com.plog.java.core.entities.UniqueEntity;
import com.plog.java.core.game.exceptions.EntityAlreadyExistsException;
import com.plog.java.core.game.exceptions.EntityNotFoundException;

import java.util.HashMap;
import java.util.UUID;

public abstract class EntityInMemoryRepository<T extends UniqueEntity> {
    private final HashMap<UUID, T> privateMap;

    public EntityInMemoryRepository() {
        privateMap = new HashMap<>();
    }

    public void add(final T entity) throws EntityAlreadyExistsException {
        if (!privateMap.containsKey(entity.getId())) {
            privateMap.put(entity.getId(), entity);
        } else {
            throw new EntityAlreadyExistsException(entity.getId());
        }
    }

    public void delete(final UUID id) throws EntityNotFoundException {
        if (privateMap.containsKey(id)) {
            privateMap.remove(id);
        } else {
            throw new EntityNotFoundException(id);
        }
    }

    public T get(final UUID id) throws EntityNotFoundException {
        if (privateMap.containsKey(id)) {
            return privateMap.get(id);
        } else {
            throw new EntityNotFoundException(id);
        }
    }

    public void update(final UUID id, final T entity) throws EntityNotFoundException {
        if (privateMap.containsKey(id)) {
            privateMap.put(id, entity);
        } else {
            throw new EntityNotFoundException(id);
        }
    }
}
