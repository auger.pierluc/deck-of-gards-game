package com.plog.java.db;

import com.plog.java.core.deck.Deck;
import com.plog.java.core.deck.DeckRepository;
import com.plog.java.core.entities.UniqueEntity;

public class DecksInMemoryRepository<T extends UniqueEntity> extends EntityInMemoryRepository<Deck> implements DeckRepository {

}
