package com.plog.java.db;

import com.plog.java.core.entities.UniqueEntity;
import com.plog.java.core.game.Game;
import com.plog.java.core.game.GamesRepository;

public class GamesInMemoryRepository<T extends UniqueEntity> extends EntityInMemoryRepository<Game> implements GamesRepository {
}
