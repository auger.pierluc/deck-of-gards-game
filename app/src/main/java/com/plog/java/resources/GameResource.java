package com.plog.java.resources;

import com.plog.java.api.game.CreateGameDto;
import com.plog.java.api.game.GameDto;
import com.plog.java.core.game.Game;
import com.plog.java.core.game.GamesRepository;
import com.plog.java.core.game.exceptions.EntityNotFoundException;
import io.dropwizard.jersey.params.UUIDParam;
import io.swagger.annotations.Api;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Api
@Path("/games")
@Produces(MediaType.APPLICATION_JSON)
public class GameResource {
    private final GamesRepository repository;

    public GameResource(final GamesRepository repository) {
        this.repository = repository;
    }

    @GET
    @Path("/{gameId}")
    public GameDto getGame(@PathParam("gameId") final UUIDParam gameId) throws EntityNotFoundException {

        final Game game = repository.get(gameId.get());

        return new GameDto(game.getId());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addGame(final CreateGameDto game, @Context final UriInfo uriInfo) throws Exception {
        final Game createdGame = new Game();
        repository.add(createdGame);

        final UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(createdGame.getId().toString());
        return Response.created(builder.build()).build();
    }

    @DELETE
    @Path("/{gameId}")
    public void deleteGame(@PathParam("gameId") final UUIDParam gameId) throws Exception {
        repository.delete(gameId.get());
    }
}