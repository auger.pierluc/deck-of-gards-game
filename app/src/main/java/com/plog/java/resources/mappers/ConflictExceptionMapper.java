package com.plog.java.resources.mappers;

import com.plog.java.core.game.exceptions.ConflictException;
import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ConflictExceptionMapper implements ExceptionMapper<ConflictException> {
    public Response toResponse(final ConflictException exception) {
        return Response.status(Response.Status.CONFLICT)
                .entity(new ErrorMessage(Response.Status.CONFLICT.getStatusCode(),
                        exception.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}