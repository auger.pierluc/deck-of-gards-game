package com.plog.java.resources;

import com.plog.java.api.deck.CreateDeckDto;
import com.plog.java.core.deck.Deck;
import com.plog.java.core.deck.DeckRepository;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

@Api
@Path("/decks")
@Produces(MediaType.APPLICATION_JSON)
public class DeckResource {
    private final DeckRepository repository;

    public DeckResource(final DeckRepository repository) {
        this.repository = repository;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDeck(final CreateDeckDto game, @Context final UriInfo uriInfo) throws Exception {
        final Deck createDeck = new Deck();
        repository.add(createDeck);

        final UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(createDeck.getId().toString());
        return Response.created(builder.build()).build();
    }
}