package com.plog.java.resources;

import com.plog.java.api.game.players.AddPlayerToGameDto;
import com.plog.java.api.game.players.DealCardDto;
import com.plog.java.core.game.Game;
import com.plog.java.core.game.GamesRepository;
import com.plog.java.core.game.Player;
import com.plog.java.core.game.PlayerScore;
import com.plog.java.core.game.exceptions.EntityNotFoundException;
import io.dropwizard.jersey.params.UUIDParam;
import io.swagger.annotations.Api;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Collection;
import java.util.List;

@Api
@Path("/games/{gameId}/players")
@Produces(MediaType.APPLICATION_JSON)
public class GamePlayersResource {
    private final GamesRepository repository;

    public GamePlayersResource(final GamesRepository repository) {
        this.repository = repository;
    }

    @GET
    public Collection<Player> getPlayers(@PathParam("gameId") final UUIDParam gameId) throws EntityNotFoundException {
        final Game game = repository.get(gameId.get());
        return game.getPlayers();
    }

    @GET
    @Path("/score")
    public List<PlayerScore> getPlayersScore(@PathParam("gameId") final UUIDParam gameId) throws EntityNotFoundException {
        final Game game = repository.get(gameId.get());
        return game.getPlayersScore();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPlayer(@PathParam("gameId") final UUIDParam gameId, final AddPlayerToGameDto body, @Context final UriInfo uriInfo) throws Exception {
        final Game game = repository.get(gameId.get());
        final Player player = new Player(body.getName());
        game.addPlayer(player);
        repository.update(game.getId(), game);

        final UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(player.getId().toString());
        return Response.created(builder.build()).build();
    }

    @DELETE
    @Path("/{playerId}")
    public void removePlayer(@PathParam("gameId") final UUIDParam gameId, @PathParam("playerId") final UUIDParam playerId) throws Exception {
        final Game game = repository.get(gameId.get());
        game.removePlayer(playerId.get());
        repository.update(game.getId(), game);
    }

    @POST
    @Path("/{playerId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void dealCardToPlayer(@PathParam("gameId") final UUIDParam gameId, @PathParam("playerId") final UUIDParam playerId, final DealCardDto body) throws Exception {
        final Game game = repository.get(gameId.get());
        game.dealCardsToPlayer(body.getNumberOfCards(), playerId.get());
        repository.update(game.getId(), game);
    }
}