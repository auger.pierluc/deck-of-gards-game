package com.plog.java.resources;

import com.plog.java.core.deck.Deck;
import com.plog.java.core.deck.DeckRepository;
import com.plog.java.core.game.Game;
import com.plog.java.core.game.GamesRepository;
import com.plog.java.core.game.Player;
import com.plog.java.core.game.PlayerNotInGameException;
import com.plog.java.core.game.exceptions.EntityAlreadyExistsException;
import com.plog.java.core.game.exceptions.EntityNotFoundException;
import com.plog.java.core.game.exceptions.NotEnoughCardsLeftToDealException;
import com.plog.java.core.game.exceptions.PlayerAlreadyInGameException;
import io.swagger.annotations.Api;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Api
@Path("/simulator")
@Produces(MediaType.APPLICATION_JSON)
public class GameSimulatorResource {
    private final GamesRepository gamesRepository;
    private final DeckRepository decksRepository;

    public GameSimulatorResource(final GamesRepository gamesRepository, final DeckRepository decksRepository) {

        this.gamesRepository = gamesRepository;
        this.decksRepository = decksRepository;
    }

    @POST
    @Path("/setupFakeGame")
    @Produces(MediaType.APPLICATION_JSON)
    public String setup() throws EntityNotFoundException, NotEnoughCardsLeftToDealException, PlayerAlreadyInGameException, EntityAlreadyExistsException {

        final Game game = new Game();

        setupGame(gamesRepository, game);

        return game.getId().toString();
    }

    public static void setupGame(final GamesRepository gamesRepository, final Game game) throws PlayerAlreadyInGameException, PlayerNotInGameException, NotEnoughCardsLeftToDealException, EntityAlreadyExistsException {
        final Deck deck = new Deck();
        final Player player1 = new Player("player1");
        final Player player2 = new Player("player2");

        game.addDeck(deck);

        game.shuffleDeck();

        game.addPlayer(player1);
        game.addPlayer(player2);
        game.dealCardsToPlayer(5, player1.getId());
        game.dealCardsToPlayer(8, player2.getId());

        gamesRepository.add(game);
    }
}