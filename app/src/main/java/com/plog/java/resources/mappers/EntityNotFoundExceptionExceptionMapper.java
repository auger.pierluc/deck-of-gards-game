package com.plog.java.resources.mappers;

import com.plog.java.core.game.exceptions.EntityNotFoundException;
import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityNotFoundExceptionExceptionMapper implements ExceptionMapper<EntityNotFoundException> {
    public Response toResponse(final EntityNotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(),
                        exception.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}

