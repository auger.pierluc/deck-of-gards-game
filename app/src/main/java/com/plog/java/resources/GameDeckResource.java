package com.plog.java.resources;

import com.plog.java.api.AddDeckToGameDto;
import com.plog.java.api.deck.CardCountDto;
import com.plog.java.api.deck.SuitCountDto;
import com.plog.java.core.deck.Deck;
import com.plog.java.core.deck.DeckRepository;
import com.plog.java.core.game.Game;
import com.plog.java.core.game.GamesRepository;
import com.plog.java.core.game.NoDeckInGameException;
import com.plog.java.core.game.exceptions.EntityNotFoundException;
import io.dropwizard.jersey.params.UUIDParam;
import io.swagger.annotations.Api;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Api
@Path("/games/{gameId}/deck")
@Produces(MediaType.APPLICATION_JSON)
public class GameDeckResource {
    private final GamesRepository gamesRepository;
    private final DeckRepository decksRepository;

    public GameDeckResource(final GamesRepository gamesRepository, final DeckRepository decksRepository) {

        this.gamesRepository = gamesRepository;
        this.decksRepository = decksRepository;
    }

    @GET
    @Path("/cards")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CardCountDto> getCardsLeftPerSuite(@PathParam("gameId") final UUIDParam gameId) throws EntityNotFoundException, NoDeckInGameException {
        final Game game = gamesRepository.get(gameId.get());
        return game.getDeck().getRemainingCardsCount().stream().map(cardCount -> new CardCountDto(cardCount.getCard(), cardCount.getCount())).collect(toList());
    }

    @GET
    @Path("/stats")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SuitCountDto> getCardCount(@PathParam("gameId") final UUIDParam gameId) throws EntityNotFoundException, NoDeckInGameException {
        final Game game = gamesRepository.get(gameId.get());
        return game.getDeck().getNonDealtCardsPerSuit().stream().map(suitCount -> new SuitCountDto(suitCount.getSuit(), suitCount.getCount())).collect(toList());
    }

    @POST
    @Path("/shuffle")
    @Consumes(MediaType.APPLICATION_JSON)
    public void shuffle(@PathParam("gameId") final UUIDParam gameId) throws Exception {
        final Game game = gamesRepository.get(gameId.get());
        game.shuffleDeck();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addDeckToGame(@PathParam("gameId") final UUIDParam gameId, final AddDeckToGameDto body) throws Exception {
        final Game game = gamesRepository.get(gameId.get());
        final Deck deck = decksRepository.get(body.getDeckId());

        game.addDeck(deck);
        gamesRepository.update(gameId.get(), game);
    }
}