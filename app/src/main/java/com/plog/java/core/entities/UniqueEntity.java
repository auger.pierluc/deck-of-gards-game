package com.plog.java.core.entities;

import java.util.UUID;

public abstract class UniqueEntity {
    private final UUID id;

    protected UniqueEntity() {
        id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }
}
