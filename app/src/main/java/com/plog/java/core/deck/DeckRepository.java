package com.plog.java.core.deck;

import com.plog.java.core.game.exceptions.EntityAlreadyExistsException;
import com.plog.java.core.game.exceptions.EntityNotFoundException;

import java.util.UUID;

public interface DeckRepository {
    void add(Deck Deck) throws EntityAlreadyExistsException;

    Deck get(UUID deckId) throws EntityNotFoundException;
}
