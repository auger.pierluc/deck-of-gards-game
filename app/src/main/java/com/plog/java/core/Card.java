package com.plog.java.core;

import java.security.InvalidParameterException;
import java.util.Objects;

public class Card implements Comparable<Card> {
    private final int value;
    private final Suit suit;

    public Card(final int value, final Suit suit) {
        if (value < 0 || value > 13) {
            throw new InvalidParameterException("Value must be between 1 and 13, included.");
        }
        this.value = value;
        this.suit = suit;
    }

    public int getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Card card = (Card) o;
        return value == card.value &&
                suit == card.suit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, suit);
    }

    @Override
    public int compareTo(final Card o) {
        final int i = suit.compareTo(o.suit);

        if (i != 0) return i;

        return Integer.compare(value, o.getValue()) * -1;
    }
}
