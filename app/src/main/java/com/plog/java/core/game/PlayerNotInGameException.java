package com.plog.java.core.game;

import com.plog.java.core.game.exceptions.EntityNotFoundException;

import java.util.UUID;

public class PlayerNotInGameException extends EntityNotFoundException {
    public PlayerNotInGameException(final UUID id) {
        super(id);
    }
}
