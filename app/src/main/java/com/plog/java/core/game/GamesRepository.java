package com.plog.java.core.game;

import com.plog.java.core.game.exceptions.EntityAlreadyExistsException;
import com.plog.java.core.game.exceptions.EntityNotFoundException;

import java.util.UUID;

public interface GamesRepository {
    void add(Game game) throws EntityAlreadyExistsException;

    void delete(UUID gameId) throws EntityNotFoundException;

    Game get(UUID id) throws EntityNotFoundException;

    void update(UUID id, Game game) throws EntityNotFoundException;
}


