package com.plog.java.core.game.exceptions;

import java.util.UUID;

public class EntityNotFoundException extends Exception {
    private final UUID id;

    public EntityNotFoundException(final UUID id) {
        this.id = id;
    }
}


