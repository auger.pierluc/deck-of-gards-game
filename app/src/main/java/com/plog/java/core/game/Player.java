package com.plog.java.core.game;

import com.plog.java.core.Card;
import com.plog.java.core.entities.UniqueEntity;

import java.util.ArrayList;
import java.util.List;

public class Player extends UniqueEntity {
    private final List<Card> cards = new ArrayList<>();
    private final String name;

    public Player(final String name) {
        this.name = name;
    }

    public void receiveCards(final List<Card> dealCards) {
        cards.addAll(dealCards);
    }

    public int getScore() {
        return cards.stream().map(card -> card.getValue()).mapToInt(i -> i.intValue()).sum();
    }

    public List<Card> getCards() {
        return cards;
    }
}
