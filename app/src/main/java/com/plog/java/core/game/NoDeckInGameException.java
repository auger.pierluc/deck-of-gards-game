package com.plog.java.core.game;

import com.plog.java.core.game.exceptions.NotEnoughCardsLeftToDealException;

public class NoDeckInGameException extends NotEnoughCardsLeftToDealException {
}
