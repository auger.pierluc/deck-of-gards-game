package com.plog.java.core;

public enum Suit {
    heart,
    spade,
    club,
    diamond
}
