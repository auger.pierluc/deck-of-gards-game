package com.plog.java.core.game;

import com.plog.java.core.deck.CardCount;
import com.plog.java.core.deck.Deck;
import com.plog.java.core.entities.UniqueEntity;
import com.plog.java.core.game.exceptions.NotEnoughCardsLeftToDealException;
import com.plog.java.core.game.exceptions.PlayerAlreadyInGameException;

import java.util.*;
import java.util.stream.Collectors;

public class Game extends UniqueEntity {

    private Optional<Deck> deck;
    private final Map<UUID, Player> players;

    public Game() {
        deck = Optional.empty();
        players = new HashMap<>();
    }

    public void addPlayer(final Player player) throws PlayerAlreadyInGameException {
        final Player value = players.get(player.getId());
        if (value == null) {
            players.put(player.getId(), player);
        } else {
            throw new PlayerAlreadyInGameException();
        }
    }

    public void dealCardsToPlayer(final int numberOfCards, final UUID playerId) throws PlayerNotInGameException, NotEnoughCardsLeftToDealException {
        checkDeck();

        final Player player = getPlayer(playerId);
        player.receiveCards(deck.get().dealCards(numberOfCards));
    }

    public List<PlayerScore> getPlayersScore() {
        final Comparator<Player> comparator = Comparator.comparingInt(Player::getScore);
        return players.values().stream().sorted(comparator.reversed()).collect(Collectors.toList())
                .stream().map(p -> new PlayerScore(p.getId(), p.getScore())).collect(Collectors.toList());
    }

    public Collection<Player> getPlayers() {
        return players.values();
    }

    public List<CardCount> getCardsLeftInGame() throws NoDeckInGameException {
        checkDeck();
        return deck.get().getRemainingCardsCount();
    }

    public void removePlayer(final UUID playerId) throws PlayerNotInGameException {
        if (!players.containsKey(playerId)) {
            throw new PlayerNotInGameException(playerId);
        }
        players.remove(playerId);
    }

    public void addDeck(final Deck deck) {
        if (this.deck.isPresent()) {
            this.deck.get().add(deck);
        } else {
            this.deck = Optional.of(deck);
        }
    }

    public Deck getDeck() throws NoDeckInGameException {
        checkDeck();
        return deck.get();
    }

    public void shuffleDeck() throws NoDeckInGameException {
        checkDeck();
        deck.get().shuffle();
    }

    private Player getPlayer(final UUID playerId) throws PlayerNotInGameException {
        if (!players.containsKey(playerId)) {
            throw new PlayerNotInGameException(playerId);
        } else {
            return players.get(playerId);
        }
    }

    private void checkDeck() throws NoDeckInGameException {
        if (!deck.isPresent()) {
            throw new NoDeckInGameException();
        }
    }
}


