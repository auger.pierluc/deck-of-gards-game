package com.plog.java.core.game;

import java.util.UUID;

public class PlayerScore {
    private final UUID id;
    private final int score;

    public PlayerScore(final UUID id, final int score) {

        this.id = id;
        this.score = score;
    }

    public UUID getId() {
        return id;
    }

    public int getScore() {
        return score;
    }
}
