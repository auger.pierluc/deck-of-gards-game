package com.plog.java.core.deck;

import com.plog.java.core.Card;
import com.plog.java.core.Suit;
import com.plog.java.core.entities.UniqueEntity;
import com.plog.java.core.game.exceptions.NotEnoughCardsLeftToDealException;

import java.util.*;

import static java.util.stream.Collectors.*;

public class Deck extends UniqueEntity {

    private final List<Card> cards = new LinkedList<>();
    public static final int NUMBER_OF_CARDS_PER_SUITE = 13;
    public static final Suit[] SUITES_IN_DECK = new Suit[]{Suit.club, Suit.spade, Suit.diamond, Suit.heart};

    public Deck() {
        initializeDeck();
    }


    public void add(final Deck deck) {
        cards.addAll(deck.cards);
    }

    public List<Card> dealCards(final int numberOfCardsToDeal) throws NotEnoughCardsLeftToDealException {
        if (cards.size() < numberOfCardsToDeal) {
            throw new NotEnoughCardsLeftToDealException();
        } else {
            final List<Card> cardToDeal = new ArrayList<>();

            for (int i = 0; i < numberOfCardsToDeal; i++) {
                cardToDeal.add(cards.get(i));
            }
            cards.removeAll(cardToDeal);

            return cardToDeal;
        }
    }


    public List<Card> getCards() {
        return cards;
    }

    public void shuffle() {
        shuffleList(cards);
    }

    public List<SuitCount> getNonDealtCardsPerSuit() {
        final Map<Suit, Long> countPerSuit = cards.stream().collect(groupingBy(c -> c.getSuit(), counting()));
        return countPerSuit.entrySet().stream().map(e -> new SuitCount(e.getKey(), e.getValue())).collect(toList());
    }

    public List<CardCount> getRemainingCardsCount() {
//        final Map<Card, Long> counts = cards.stream().collect(groupingBy(s -> s, counting()));
//
//        counts.entrySet().stream().sorted(c -> c.getKey().getSuit());
//        cards.sort(Comparator.reverseOrder());
//
//        counts.entrySet().stream().sorted(Comparator.comparing(c -> c.getKey().getValue()));
//
//        return new TreeMap<>(counts);

        final Map<Card, Long> counts = cards.stream().collect(groupingBy(s -> s, counting()));
        final List<CardCount> cardCounts = counts.entrySet().stream().map(c -> new CardCount(c.getKey(), c.getValue())).collect(toList());
        Collections.sort(cardCounts);
        return cardCounts;
    }

    private void initializeDeck() {
        for (final Suit suit : SUITES_IN_DECK) {
            for (int cardNumber = 1; cardNumber <= NUMBER_OF_CARDS_PER_SUITE; cardNumber++) {
                cards.add(new Card(cardNumber, suit));
            }
        }
    }

    private static void shuffleList(final List<Card> cards) {
        final int n = cards.size();
        final Random random = new Random();
        random.nextInt();
        for (int index = 0; index < n; index++) {
            final int change = index + random.nextInt(n - index);
            swap(cards, index, change);
        }
    }

    private static void swap(final List<Card> cards, final int indexOfCardToSwap, final int change) {
        final Card cardToSwap = cards.get(indexOfCardToSwap);
        cards.set(indexOfCardToSwap, cards.get(change));
        cards.set(change, cardToSwap);
    }
}

