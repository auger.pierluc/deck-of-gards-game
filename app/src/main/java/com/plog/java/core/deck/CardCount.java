package com.plog.java.core.deck;

import com.plog.java.core.Card;

public class CardCount implements Comparable<CardCount> {
    private final Card card;
    private final Long count;

    public CardCount(final Card card, final Long count) {
        this.card = card;
        this.count = count;
    }

    public Card getCard() {
        return card;
    }

    public Long getCount() {
        return count;
    }

    @Override
    public int compareTo(final CardCount o) {
        return card.compareTo(o.getCard());
    }
}
