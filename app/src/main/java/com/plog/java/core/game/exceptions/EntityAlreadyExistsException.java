package com.plog.java.core.game.exceptions;

import java.util.UUID;

public class EntityAlreadyExistsException extends ConflictException {
    private final UUID id;

    public EntityAlreadyExistsException(final UUID id) {
        this.id = id;
    }
}
