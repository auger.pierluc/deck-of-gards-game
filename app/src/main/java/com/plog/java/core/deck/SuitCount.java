package com.plog.java.core.deck;

import com.plog.java.core.Suit;

public class SuitCount {
    private final Suit suit;
    private final Long count;

    public SuitCount(final Suit suit, final Long count) {
        this.suit = suit;
        this.count = count;
    }

    public Suit getSuit() {
        return suit;
    }

    public Long getCount() {
        return count;
    }
}
