package com.plog.java;

import com.plog.java.core.deck.Deck;
import com.plog.java.core.game.GamesRepository;
import com.plog.java.db.DecksInMemoryRepository;
import com.plog.java.db.GamesInMemoryRepository;
import com.plog.java.resources.*;
import com.plog.java.resources.mappers.ConflictExceptionMapper;
import com.plog.java.resources.mappers.EntityNotFoundExceptionExceptionMapper;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class GameApplication extends Application<GameConfiguration> {


    public static void main(final String[] args) throws Exception {
        new GameApplication().run(args);
    }

    @Override
    public String getName() {
        return "game";
    }

    @Override
    public void run(final GameConfiguration configuration,
                    final Environment environment) {
        final GamesRepository gamesRepository = new GamesInMemoryRepository<>();
        final DecksInMemoryRepository<Deck> deckRepository = new DecksInMemoryRepository<>();


        environment.jersey().register(new ConflictExceptionMapper());
        environment.jersey().register(new EntityNotFoundExceptionExceptionMapper());

        environment.jersey().register(new GameResource(gamesRepository));
        environment.jersey().register(new GameSimulatorResource(gamesRepository, deckRepository));
        environment.jersey().register(new GamePlayersResource(gamesRepository));
        environment.jersey().register(new DeckResource(deckRepository));
        environment.jersey().register(new GameDeckResource(gamesRepository, deckRepository));
    }


    @Override
    public void initialize(final Bootstrap<GameConfiguration> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<GameConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(final GameConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }
}
