package com.plog.java.api.game.players;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DealCardDto {
    private int numberOfCards;

    public DealCardDto() {
    }

    public DealCardDto(final int numberOfCards) {
        this.numberOfCards = numberOfCards;
    }

    @JsonProperty
    public void setNumberOfCards(final int numberOfCards) {
        this.numberOfCards = numberOfCards;
    }

    @JsonProperty
    public int getNumberOfCards() {
        return numberOfCards;
    }
}
