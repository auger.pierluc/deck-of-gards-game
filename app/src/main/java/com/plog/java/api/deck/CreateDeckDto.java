package com.plog.java.api.deck;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateDeckDto {
    private String name;

    @JsonProperty
    public void setName(final String name) {
        this.name = name;
    }

    @JsonProperty
    public String getName() {
        return name;
    }
}
