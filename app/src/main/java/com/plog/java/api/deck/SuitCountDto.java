package com.plog.java.api.deck;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.plog.java.core.Suit;

public class SuitCountDto {
    private final Suit suit;
    private final Long count;

    public SuitCountDto(final Suit suit, final Long count) {
        this.suit = suit;
        this.count = count;
    }

    @JsonProperty
    public Suit getSuit() {
        return suit;
    }

    @JsonProperty
    public Long getCount() {
        return count;
    }
}
