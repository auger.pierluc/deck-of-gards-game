package com.plog.java.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class AddDeckToGameDto {
    private UUID deckId;

    public AddDeckToGameDto() {
    }

    public AddDeckToGameDto(final String deckId) {
        this.deckId = UUID.fromString(deckId);
    }

    @JsonProperty
    public UUID getDeckId() {
        return deckId;
    }

    @JsonProperty
    public void setDeckId(final UUID deckId) {
        this.deckId = deckId;
    }
}
