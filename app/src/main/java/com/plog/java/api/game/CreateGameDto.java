package com.plog.java.api.game;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateGameDto {
    private String name;

    public CreateGameDto() {
    }

    public CreateGameDto(final String name) {
        this.name = name;
    }


    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(final String name) {
        this.name = name;
    }
}
