package com.plog.java.api.deck;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.plog.java.core.Card;

public class CardCountDto {
    private final String card;
    private final Long count;

    public CardCountDto(final Card card, final Long count) {
        this.card = String.format("%d %s", card.getValue(), card.getSuit().toString());
        this.count = count;
    }

    @JsonProperty
    public String getCard() {
        return card;
    }

    @JsonProperty
    public Long getCount() {
        return count;
    }
}
