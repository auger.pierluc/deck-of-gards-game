package com.plog.java.api.game.players;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddPlayerToGameDto {
    private String name;

    @JsonProperty
    public void setName(final String name) {
        this.name = name;
    }

    @JsonProperty
    public String getName() {
        return name;
    }
}
