package com.plog.java.api.game;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class GameDto {
    private UUID id;

    public GameDto() {
    }

    public GameDto(final UUID id) {

        this.id = id;
    }

    @JsonProperty
    public UUID getId() {
        return id;
    }

    @JsonProperty
    public void setId(final UUID id) {
        this.id = id;
    }
}
