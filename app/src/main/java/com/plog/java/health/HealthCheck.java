package com.plog.java.health;

public class HealthCheck extends com.codahale.metrics.health.HealthCheck {

    @Override
    protected Result check() {

        return Result.healthy();
    }
}